#include "HumanConsolePlayer.h"
#include <iostream>

void HumanConsolePlayer::UpdatePresentation()
{
	presenter.DrawFields(own_field, enemy_field);
}

vector2 HumanConsolePlayer::GetNextShotLocation()
{
	int x, y;
	char c;

	bool input_valid = false;

	do
	{
		std::cout << "Where to shoot? (e.g. 1 a): ";
		std::cin >> y >> c;

		input_valid = y > 0 && y < 11 && TryTranslateLetterToNumber(c, x);

		if (!input_valid)
			std::cout << "Input invalid!!!" << std::endl;

	}while (!input_valid);

	return vector2(x, y - 1);
}

void HumanConsolePlayer::ReportShotResult(ShotResult shotResult)
{
}

bool HumanConsolePlayer::TryTranslateLetterToNumber(char c, int & index)
{
	if (c >= 'A' && c <= 'J')
	{
		index = c - 'A';
		return true;
	}

	if (c >= 'a' && c <= 'j')
	{
		index = c - 'a';
		return true;
	}

	return false;
}
