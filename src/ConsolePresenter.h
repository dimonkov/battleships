#ifndef CONSOLE_PRESENTER_H_
#define CONSOLE_PRESENTER_H_

#include "FieldView.h"
#include <memory>

//Presents own playing field as well as the enemy one on the console
class ConsolePresenter {
public:
	void DrawFields(std::shared_ptr<FieldView> own_field, std::shared_ptr<FieldView> enemy_field);

private:
	void PrintHorizontalEmptySpace();
	void PrintSingleHeader();
	void PrintHeader();
};

#endif // !CONSOLE_PRESENTER_H_
