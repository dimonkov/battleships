#include "Ship.h"
#include <string>
#include <stdexcept>

Ship::Ship(area a)
{
	if (a.A.X != a.B.X && a.A.Y != a.B.Y)
		throw std::runtime_error("Ship configuration is invalid");

	ship_area = a;
	left_hp = this->GetTotalHitpoints();
}

int Ship::GetTotalHitpoints() const
{
	if (ship_area.A.X == ship_area.B.X)
		return std::abs(ship_area.A.Y - ship_area.B.Y) + 1;

	return std::abs(ship_area.A.X - ship_area.B.X) + 1;
}

int Ship::GetLeftHP() const
{
	return left_hp;
}

bool Ship::is_alive() const
{
	return left_hp > 0;
}

area Ship::get_area() const
{
	return ship_area;
}

void Ship::Damage(int amount)
{
	left_hp -= amount;
}
