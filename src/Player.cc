#include "Player.h"

void Player::SetOwnFieldView(std::shared_ptr<FieldView> fieldView)
{
	own_field = fieldView;
}

void Player::SetEnemyFieldView(std::shared_ptr<FieldView> fieldView)
{
	enemy_field = fieldView;
}

Player::~Player()
{
}
