#ifndef SHOT_RESULT_H_
#define SHOT_RESULT_H_

#include "vector2.h"
#include "CellStatus.h"

//Data container for shot result
class ShotResult {
public:
	CellStatus cell_status;
	vector2 coord;
};

#endif // !SHOT_RESULT_H_
