#include "Game.h"
#include <ctime>

Game::Game() : rn(time(NULL))
{
	this->Reset();
}

void Game::set_player_1(std::shared_ptr<Player> player1)
{
	this->player1 = player1;
	std::shared_ptr<FieldView> full = std::shared_ptr<FieldView>(new FieldViewFull(player_1_field));
	std::shared_ptr<FieldView> fog = std::shared_ptr<FieldView>(new FieldViewFogOfWar(player_2_field));

	player1->SetOwnFieldView(full);
	player1->SetEnemyFieldView(fog);

	player1->UpdatePresentation();
}

void Game::set_player_2(std::shared_ptr<Player> player2)
{
	this->player2 = player2;
	std::shared_ptr<FieldView> full = std::shared_ptr<FieldView>(new FieldViewFull(player_2_field));
	std::shared_ptr<FieldView> fog = std::shared_ptr<FieldView>(new FieldViewFogOfWar(player_1_field));

	player2->SetOwnFieldView(full);
	player2->SetEnemyFieldView(fog);

	player2->UpdatePresentation();
}

void Game::GameStep()
{
	int left_hp = 0;
	if (player_1_turn)
	{
		vector2 shot_loc = player1->GetNextShotLocation();

		player_2_field.Shoot(shot_loc);

		ShotResult res;

		res.coord = shot_loc;
		res.cell_status = player_2_field.get_cell_status(shot_loc);

		left_hp = player_2_field.GetLeftHitPoints();

		player1->ReportShotResult(res);
	}
	else
	{
		vector2 shot_loc = player2->GetNextShotLocation();

		player_1_field.Shoot(shot_loc);

		ShotResult res;

		res.coord = shot_loc;
		res.cell_status = player_1_field.get_cell_status(shot_loc);

		left_hp = player_1_field.GetLeftHitPoints();

		player2->ReportShotResult(res);
	}

	player1->UpdatePresentation();
	player2->UpdatePresentation();

	if (left_hp <= 0)
	{
		winner = player_1_turn ? 1 : 2;
		ended = true;
	}

	player_1_turn = !player_1_turn;
}

void Game::Reset()
{
	player_1_turn = true;
	ended = false;
	winner = -1;

	player_1_field.ClearField();
	rn.PlaceShips(player_1_field);
	player_2_field.ClearField();
	rn.PlaceShips(player_2_field);
	
	if(player1)
		player1->UpdatePresentation();
	
	if(player2)
		player2->UpdatePresentation();
}

int Game::GetWinner() const
{
	return winner;
}

bool Game::get_is_ended() const
{
	return ended;
}

int Game::GetPlayer1HP() const
{
	return player_1_field.GetLeftHitPoints();
}

int Game::GetPlayer2HP() const
{
	return player_2_field.GetLeftHitPoints();
}
