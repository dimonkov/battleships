#ifndef AREA_H_
#define AREA_H_


#include "vector2.h"


//represents a rectangular area.
//A - upper left corner, B - lower right corner
class area
{
public:
	area();
	area(vector2 a, vector2 b);

	vector2 A, B;

	bool Intersects(vector2 coord) const;

	void GrowToInclude(vector2 coord);
};

#endif // !AREA_H_