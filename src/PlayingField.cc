#include "PlayingField.h"

CellStatus PlayingField::get_cell_status(vector2 coord) const
{
	return sea[coord.X + coord.Y * PlayingField::WIDTH];
}

int PlayingField::GetLeftHitPoints() const
{
	int score = 0;

    for(Ship sh : ships)
	{
		score += sh.GetLeftHP();
	}

	return score;
}

void PlayingField::Shoot(vector2 coord)
{
	CellStatus current_status = this->get_cell_status(coord);

	if (current_status != CellStatus::SEA && current_status != CellStatus::SHIP_OK)
		return;

	for(Ship& sh : ships)
	{
		if (sh.is_alive() && sh.get_area().Intersects(coord))
		{
			sh.Damage(1);

			if (sh.is_alive())
				this->set_cell_status(coord, CellStatus::SHIP_HIT);

			else
				this->fill_area(sh.get_area(), CellStatus::SHIP_DEAD);

			break;
		}
	}

	
	if (current_status == CellStatus::SEA)
		this->set_cell_status(coord, CellStatus::MISS);
}

void PlayingField::fill_area(area ar, CellStatus status)
{
	for (int y = ar.A.Y; y <= ar.B.Y; y++)
	{
		for (int x = ar.A.X; x <= ar.B.X; x++)
		{
			this->set_cell_status(vector2(x, y), status);
		}
	}
}

void PlayingField::set_cell_status(vector2 coord, CellStatus status)
{
	sea[coord.X + coord.Y * PlayingField::WIDTH] = status;
}

PlayingField::PlayingField()
{
	this->ClearField();
}

void PlayingField::ClearField()
{
	for (int i = 0; i < PlayingField::WIDTH * PlayingField::HEIGHT; i++)
	{
		sea[i] = CellStatus::SEA;
	}

	ships.clear();
}

void PlayingField::PlaceShip(Ship sh)
{
	ships.push_back(sh);

	this->fill_area(sh.get_area(), CellStatus::SHIP_OK);
}
