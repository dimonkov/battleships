#ifndef HUMAN_CONSOLE_PLAYER_H_
#define HUMAN_CONSOLE_PLAYER_H_

#include "Player.h"
#include "ConsolePresenter.h"

//Human controlled console player
class HumanConsolePlayer : public Player {

public:
	virtual void UpdatePresentation() override;
	virtual vector2 GetNextShotLocation() override;
	virtual void ReportShotResult(ShotResult shotResult) override;

private:
	ConsolePresenter presenter;

	bool TryTranslateLetterToNumber(char c, int& index);
};

#endif // !HUMAN_CONSOLE_PLAYER_H_
