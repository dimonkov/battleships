#ifndef FIELD_VIEW_FULL_H_
#define FIELD_VIEW_FULL_H_

#include "CellStatus.h"
#include "vector2.h"
#include "FieldView.h"
#include "PlayingField.h"

//Full field view, passes everything as is
class FieldViewFull : public FieldView {
public:
	FieldViewFull(PlayingField& field);

	virtual CellStatus GetCellStatus(const vector2 coord) override;

private:
	PlayingField& field;
};

#endif // !FIELD_VIEW_FULL_H_
