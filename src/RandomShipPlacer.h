#ifndef RANDOM_SHIP_PLACER_H
#define RANDOM_SHIP_PLACER_H

#include <random>
#include "PlayingField.h"
#include "vector2.h"

//Randomly places ships onto the playing field according to the rules 
class RandomShipPlacer {
public:
	RandomShipPlacer(int seed);
	void PlaceShips(PlayingField& field);

private:
	std::pair<vector2, vector2> GenerateShipInsideTheField(const PlayingField& field, unsigned int length);
	bool CheckArea(const PlayingField& field, vector2 a, vector2 b) const;

	std::default_random_engine generator;
	std::uniform_int_distribution<unsigned int> distribution;
};

#endif // !RANDOM_SHIP_PLACER_H
