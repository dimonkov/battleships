#ifndef PLAYING_FIELD_H_
#define PLAYING_FIELD_H_

#include <array>
#include <vector>

#include "CellStatus.h"
#include "Ship.h"

//Playing field, used co quarry cell data, get ship status and shoot
class PlayingField
{
public:

	PlayingField();

	static const unsigned int WIDTH = 10;
	static const unsigned int HEIGHT = 10;

	void ClearField();
	void PlaceShip(Ship sh);
	void Shoot(vector2 coord);

	CellStatus get_cell_status(vector2 coord) const;

	int GetLeftHitPoints() const;
	
private:

	void fill_area(area ar, CellStatus status);
	void set_cell_status(vector2 coord, CellStatus status);

	std::array<CellStatus, PlayingField::WIDTH * PlayingField::HEIGHT> sea;
	std::vector<Ship> ships;
};
#endif // !PLAYING_FIELD_H_