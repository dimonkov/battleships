#include "FieldViewFogOfWar.h"

FieldViewFogOfWar::FieldViewFogOfWar(PlayingField & field) : field(field)
{
}

CellStatus FieldViewFogOfWar::GetCellStatus(const vector2 coord)
{
	CellStatus status = field.get_cell_status(coord);
	if (status == CellStatus::SHIP_OK)
		return CellStatus::SEA;

	return status;
}
