#include <iostream>
#include "HumanConsolePlayer.h"
#include "AIPlayer.h"
#include "Game.h"


using namespace std;

int main()
{
	std::shared_ptr<Player> player1 = std::shared_ptr<Player>(new HumanConsolePlayer);
	std::shared_ptr<Player> player2 = std::shared_ptr<Player>(new AIPlayer);
	Game game;
	game.set_player_1(player1);
	game.set_player_2(player2);

	while (!game.get_is_ended())
	{
		game.GameStep();
	}

	std::cout << "Winner - Player: " << game.GetWinner() << std::endl;

	return 0;
}
