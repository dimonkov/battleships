#include "FieldViewFull.h"

FieldViewFull::FieldViewFull(PlayingField & field) : field(field)
{
}

CellStatus FieldViewFull::GetCellStatus(const vector2 coord)
{
	return field.get_cell_status(coord);
}
