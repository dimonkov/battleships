#ifndef GAME_H_
#define GAME_H_

#include <memory>
#include <string>

#include "Player.h"
#include "PlayingField.h"
#include "FieldView.h"
#include "FieldViewFogOfWar.h"
#include "FieldViewFull.h"
#include "RandomShipPlacer.h"

//Maing game logic class with turn management
class Game {

public:

	Game();

	void set_player_1(std::shared_ptr<Player> player1);
	void set_player_2(std::shared_ptr<Player> player2);

	void GameStep();

	void Reset();

	int GetWinner() const;

	int GetPlayer1HP() const;
	int GetPlayer2HP() const;

	bool get_is_ended() const;

private:
	bool player_1_turn;
	bool ended;
	int winner;

	PlayingField player_1_field;
	PlayingField player_2_field;

	std::shared_ptr<Player> player1;
	std::shared_ptr<Player> player2;

	RandomShipPlacer rn;
};

#endif // !GAME_H_
