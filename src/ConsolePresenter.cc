#include "ConsolePresenter.h"
#include "PlayingField.h"
#include <iostream>
#include <memory>


void ConsolePresenter::DrawFields(std::shared_ptr<FieldView> own_field, std::shared_ptr<FieldView> enemy_field)
{
	for (int i = 0; i < 10; i++)
		std::cout << std::endl;

	this->PrintHeader();

	for (int y = 0; y < PlayingField::HEIGHT; y++)
	{
		std::cout << y + 1 << "  ";
		if (y < PlayingField::HEIGHT - 1)
			std::cout << " ";


		for (int x = 0; x < PlayingField::WIDTH; x++)
		{
			std::cout << (char)own_field->GetCellStatus(vector2(x, y)) << ' ';
		}

		PrintHorizontalEmptySpace();

		std::cout << y + 1 << "  ";
		if (y < PlayingField::HEIGHT - 1)
			std::cout << " ";

		for (int x = 0; x < PlayingField::WIDTH; x++)
		{
			std::cout << (char)enemy_field->GetCellStatus(vector2(x, y)) << ' ';
		}
		std::cout << std::endl;
	}

	return;
}

void ConsolePresenter::PrintHorizontalEmptySpace()
{
	std::cout << "               |                ";
}

void ConsolePresenter::PrintSingleHeader()
{
	std::cout << "   ";
	for (int i = 'A'; i < 'A' + 10; i++)
	{
		std::cout << (char)i << ' ';
	}
}

void ConsolePresenter::PrintHeader()
{
	std::cout << " ";
	PrintSingleHeader();
	PrintHorizontalEmptySpace();
	std::cout << " ";
	PrintSingleHeader();
	std::cout << std::endl;
}
