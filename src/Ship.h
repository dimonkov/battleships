#ifndef SHIP_H_
#define SHIP_H_

#include "area.h"

//Ship with it's information and taken area
class Ship {
public:
	Ship(area a);

	int GetTotalHitpoints() const;
	int GetLeftHP() const;
	bool is_alive() const;
	area get_area() const;

	void Damage(int amount);
	
private:
	area ship_area;
	int left_hp;
};

#endif // !SHIP_H_
