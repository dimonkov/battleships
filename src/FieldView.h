#ifndef FIELD_VIEW_H_
#define FIELD_VIEW_H_

#include "PlayingField.h"
#include "vector2.h"

//Abstract base class for every FieldView
class FieldView {
public:
	virtual CellStatus GetCellStatus(const vector2 coord) = 0;

	virtual ~FieldView();
};

#endif // !FIELD_VIEW_H_
