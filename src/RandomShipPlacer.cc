#include "RandomShipPlacer.h"
#include <algorithm>

RandomShipPlacer::RandomShipPlacer(int seed) 
{
	generator = std::default_random_engine(seed);
}

void RandomShipPlacer::PlaceShips(PlayingField & field)
{
	for (int length = 4; length > 0; length--)
	{
		for (int count = 5 - length; count > 0;)
		{
			std::pair<vector2, vector2> bb = this->GenerateShipInsideTheField(field, length);

			if (this->CheckArea(field, bb.first, bb.second))
			{
				area ar(bb.first, bb.second);

				Ship sh(ar);

				field.PlaceShip(sh);

				count--;
			}
		}
	}
}

std::pair<vector2, vector2> RandomShipPlacer::GenerateShipInsideTheField(const PlayingField & field, unsigned int length)
{
	bool vertical = this->distribution(generator) % 2;

	if (vertical)
	{
		int y = distribution(generator) % (PlayingField::HEIGHT - length + 1);
		int x = distribution(generator) % PlayingField::WIDTH;

		return std::pair<vector2, vector2>(vector2(x, y), vector2(x, y + length - 1));
	}
	else
	{
		int y = distribution(generator) % PlayingField::HEIGHT;
		int x = distribution(generator) % (PlayingField::WIDTH - length + 1);

		return std::pair<vector2, vector2>(vector2(x, y), vector2(x + length - 1, y));
	}
}

bool RandomShipPlacer::CheckArea(const PlayingField& field, vector2 a, vector2 b) const
{
	//Enlarge the bounding box for check
	int aX = std::max(0, a.X - 1);
	int aY = std::max(0, a.Y - 1);

	int bX = std::min((int)PlayingField::WIDTH - 1, b.X + 1);
	int bY = std::min((int)PlayingField::HEIGHT - 1, b.Y + 1);

	for (int y = aY; y <= bY; y++)
	{
		for (int x = aX; x <= bX; x++)
		{
			if (field.get_cell_status(vector2(x, y)) != CellStatus::SEA)
				return false;
		}
	}

	return true;
}
