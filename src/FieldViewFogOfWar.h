#ifndef FIELD_VIEW_FOG_OF_WAR_H_
#define FIELD_VIEW_FOG_OF_WAR_H_

#include "CellStatus.h"
#include "vector2.h"
#include "FieldView.h"
#include "PlayingField.h"

//FieldView that hides the ships
class FieldViewFogOfWar : public FieldView {
public:
	FieldViewFogOfWar(PlayingField& field);

	virtual CellStatus GetCellStatus(const vector2 coord) override;

private:
	PlayingField& field;
};


#endif // !FIELD_VIEW_FOG_OF_WAR_H_
