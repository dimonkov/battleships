#include "area.h"
#include <algorithm>

area::area() : A(vector2(0,0)), B(vector2(0,0)) { }

area::area(vector2 a, vector2 b) 
{
	int minx = std::min(a.X, b.X);
	int maxx = std::max(a.X, b.X);

	int miny = std::min(a.Y, b.Y);
	int maxy = std::max(a.Y, b.Y);

	this->A = vector2(minx, miny);
	this->B = vector2(maxx, maxy);
}

bool area::Intersects(vector2 coord) const
{
	return coord.X >= A.X && coord.X <= B.X && coord.Y >= A.Y && coord.Y <= B.Y;
}

void area::GrowToInclude(vector2 coord)
{
	int minx = std::min(A.X, coord.X);
	int maxx = std::max(B.X, coord.X);

	int miny = std::min(A.Y, coord.Y);
	int maxy = std::max(B.Y, coord.Y);

	this->A = vector2(minx, miny);
	this->B = vector2(maxx, maxy);
}
