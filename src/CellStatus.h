#ifndef CELL_STATUS_H_
#define CELL_STATUS_H_

//Represents every state that the playing field can be in
enum class CellStatus : char
{
	SEA = '~',
	SHIP_OK = 'O',
	SHIP_HIT = '@',
	SHIP_DEAD = 'X',
	MISS = '.'
};

#endif // !CELL_STATUS_H_