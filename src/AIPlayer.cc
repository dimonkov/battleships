#include "AIPlayer.h"
#include "PlayingField.h"
#include <ctime>


AIPlayer::AIPlayer()
{
	play_area = area(vector2(0, 0), vector2(PlayingField::WIDTH - 1, PlayingField::HEIGHT - 1));
	generator = std::default_random_engine(time(NULL));
}

void AIPlayer::UpdatePresentation()
{
}

vector2 AIPlayer::GetNextShotLocation()
{
	if (shot_results.size() == 0)
		return ShootWithNoClue();
	else if (shot_results.size() == 1)
		return ShootAround();

	return ShootDirectional();
}

void AIPlayer::ReportShotResult(ShotResult shotResult)
{
	if (shotResult.cell_status == CellStatus::SHIP_HIT)
	{
		if (shot_results.size() == 0)
		{
			hit_area = area(shotResult.coord, shotResult.coord);
		}

		hit_area.GrowToInclude(shotResult.coord);
		shot_results.push_back(shotResult);
	}
	else if (shotResult.cell_status == CellStatus::SHIP_DEAD)
	{
		hit_area = area();
		shot_results.clear();
	}
		
}

std::pair<vector2, vector2> AIPlayer::GenerateRandomFirstShotLocation(vector2& shot_loc)
{
	int x = distribution(generator) % PlayingField::WIDTH;
	int y = distribution(generator) % PlayingField::HEIGHT;

	int aX = std::max(0, x - 1);
	int aY = std::max(0, y - 1);

	int bX = std::min(int(PlayingField::WIDTH - 1), x + 1);
	int bY = std::min(int(PlayingField::HEIGHT - 1), y + 1);

	shot_loc = vector2(x, y);

	//First hit can't have anything in 3x3 area cross pattern
	return std::pair<vector2, vector2>(vector2(aX, aY), vector2(bX, bY));
}

bool AIPlayer::CheckArea(std::pair<vector2, vector2> bb, vector2 shot_loc)
{
	for (int y = bb.first.Y; y <= bb.second.Y; y++)
	{
		for (int x = bb.first.X; x <= bb.second.X; x++)
		{
			if (x != shot_loc.X && y != shot_loc.Y)
				continue;

			CellStatus st = enemy_field->GetCellStatus(vector2(x, y));
			if (st == CellStatus::SHIP_HIT || st == CellStatus::SHIP_DEAD)
				return false;
		}
	}

	return true;
}

vector2 AIPlayer::ShootWithNoClue()
{
	
	vector2 shot_loc;
	while (true)
	{
		std::pair<vector2, vector2> bb = this->GenerateRandomFirstShotLocation(shot_loc);
		if (CheckArea(bb, shot_loc))
		{
			return shot_loc;
		}
	}
}

vector2 AIPlayer::ShootAround()
{
	ShotResult last_shot = shot_results[0];

	vector2 l = last_shot.coord;

	if (l.X + 1 < PlayingField::WIDTH && enemy_field->GetCellStatus(vector2(l.X + 1, l.Y)) == CellStatus::SEA)
		return vector2(l.X + 1, l.Y);

	if (l.X - 1 >= 0 && enemy_field->GetCellStatus(vector2(l.X - 1, l.Y)) == CellStatus::SEA)
		return vector2(l.X - 1, l.Y);

	if (l.Y + 1 < PlayingField::HEIGHT && enemy_field->GetCellStatus(vector2(l.X, l.Y + 1)) == CellStatus::SEA)
		return vector2(l.X, l.Y + 1);

	if (l.Y - 1 >= 0 && enemy_field->GetCellStatus(vector2(l.X, l.Y - 1)) == CellStatus::SEA)
		return vector2(l.X, l.Y - 1);

	shot_results.clear();

	hit_area = area();

	return ShootWithNoClue();
}

vector2 AIPlayer::ShootDirectional()
{
	//Positive val
	vector2 vec = hit_area.B - hit_area.A;
	int l = vec.X + vec.Y;

	vec = vec / l;

	vector2 side = hit_area.A - vec;

	if (play_area.Intersects(side) && enemy_field->GetCellStatus(side) == CellStatus::SEA)
		return side;

	side = hit_area.B + vec;
	if (play_area.Intersects(side) && enemy_field->GetCellStatus(side) == CellStatus::SEA)
		return side;

	shot_results.clear();

	hit_area = area();

	return ShootWithNoClue();
}
