#ifndef PLAYER_H_
#define PLAYER_H_

#include "FieldView.h"
#include "vector2.h"
#include "ShotResult.h"

#include <memory>

//Abstract base class for all players
class Player {

public:
	void SetOwnFieldView(std::shared_ptr<FieldView> fieldView);
	void SetEnemyFieldView(std::shared_ptr<FieldView> fieldView);

	virtual void UpdatePresentation() = 0;
	virtual vector2 GetNextShotLocation() = 0;
	virtual void ReportShotResult(ShotResult shotResult) = 0;
	virtual ~Player();

protected:
	std::shared_ptr<FieldView> own_field;
	std::shared_ptr<FieldView> enemy_field;
};

#endif // !PLAYER_H_
