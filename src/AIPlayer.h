#ifndef AIPLAYER_H_
#define AIPLAYER_H_

#include "FieldView.h"
#include "vector2.h"
#include "Player.h"
#include <memory>
#include <vector>
#include <random>

//Non human controlled player that can play the game and shoot strategically
class AIPlayer : public Player {

public:
	AIPlayer();
	virtual void UpdatePresentation() override;
	virtual vector2 GetNextShotLocation() override;
	virtual void ReportShotResult(ShotResult shotResult) override;

private:
	std::default_random_engine generator;
	std::uniform_int_distribution<unsigned int> distribution;

	area hit_area;
	area play_area;

	std::vector<ShotResult> shot_results;


	std::pair<vector2, vector2> GenerateRandomFirstShotLocation(vector2& shot_loc);
	bool CheckArea(std::pair<vector2, vector2> bb, vector2 shot_loc);

	vector2 ShootWithNoClue();
	vector2 ShootAround();
	vector2 ShootDirectional();
};


#endif // !AIPLAYER_H_
