#ifndef VECTOR2_H_
#define VECTOR2_H_

//Two coordinates representing a point on the plane
class vector2
{
public:
	vector2(int x, int y);
	vector2();
	int X, Y;

	vector2 operator+(const vector2& vec) const;
	vector2 operator-(const vector2& vec) const;
	vector2 operator*(int n) const;
	vector2 operator/(int n) const;
};

#endif // !VECTOR2_H_